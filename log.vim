" Vim syntax file
" Language: log files (.log)
" URL: https://gitlab.com/lomax/misc/blob/master/log.vim
" Last Change: 2018 Aug 07

if exists("b:current_syntax")
  finish
endif

set background=dark

hi colorGrey        cterm=NONE ctermbg=NONE     ctermfg=Grey
hi colorRed         cterm=NONE ctermbg=NONE     ctermfg=DarkRed

syn match patDebug      "^[0-9-: ,]* DEBUG .*$"
syn match patWarn       "^[0-9-: ,]* WARN .*$"
syn match patWarn       "^[0-9-: ,]* WARNING .*$"
syn match patError      "^[0-9-: ,]* ERROR .*$"
syn match patFatal      "^[0-9-: ,]* FATAL .*$"
syn match patFatal      "^[0-9-: ,]* SEVERE .*$"
syn match patStackElem  "^[0-9-: ,]*[ \t]at "
syn match patException  "\w*Exception"
syn match patClass      "\[.*\]"

hi def link patDebug      PmenuSel
hi def link patWarn       Statement
hi def link patError      colorRed
hi def link patFatal      Error
hi def link patStackElem  Constant
hi def link patException  Constant
hi def link patClass      colorGrey

let b:current_syntax = "log"